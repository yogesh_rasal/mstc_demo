/*

Parent Exe

*/


#include <windows.h> 
#include <tchar.h>
#include <stdio.h> 
#include <strsafe.h>
#include "resource.h"

#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "Winmm.lib")


int MUSIC_PLAY_STATUS=TRUE;


// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) 
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	//wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	 wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON ));
	 wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON ));

	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);


	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName,
		TEXT("Journey of MSTC..."),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	//AnimateWindow(hwnd , 2000 , AW_CENTER);
    
	SetWindowLong(hwnd,GWL_STYLE,0);
	SetWindowPos(hwnd, HWND_TOP , 0, 0, 1920, 1080, SWP_FRAMECHANGED);
	// 8  memset
	ShowWindow(hwnd, iCmdShow);
	
	//ShowWindow(hwnd, SW_MAXIMIZE);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) 
{
	HDC hdc;
	HDC hdcComp;
	PAINTSTRUCT ps;
	HINSTANCE hInst;
	static HBITMAP hbmap;
	HGDIOBJ prevHGDIObj = NULL;
	BITMAP bmBuffer;
	RECT rc;
	
	
	//This is for child process start
	
		STARTUPINFO si = { sizeof(STARTUPINFO) };
		si.cb = sizeof(si);
		si.dwFlags = STARTF_USESHOWWINDOW;
		si.wShowWindow = SW_MAXIMIZE;
		PROCESS_INFORMATION pi;
		
	//	This is for child process END;
    
    //LPTSTR Cmdline[] = TEXT("\flame.exe\" -L -S"));

    /*
    BOOL CreateProcessA(
        LPCSTR                lpApplicationName,
        LPSTR                 lpCommandLine,
        LPSECURITY_ATTRIBUTES lpProcessAttributes,
        LPSECURITY_ATTRIBUTES lpThreadAttributes,
        BOOL                  bInheritHandles,
        DWORD                 dwCreationFlags,
        LPVOID                lpEnvironment,
        LPCSTR                lpCurrentDirectory,
        LPSTARTUPINFOA        lpStartupInfo,
        LPPROCESS_INFORMATION lpProcessInformation
    );
            */

	// code
	switch (iMsg) 
	{
	case WM_CREATE:
		hInst = (HINSTANCE)((LPCREATESTRUCT)lParam)->hInstance;
		//hbmap = LoadBitmap(hInst, MAKEINTRESOURCE(MY_BITMAP));
           
		
		CreateProcessA(
            LPCSTR(TEXT("C:\\wnd\\MSTC_SPLASH_SCREEN.exe")), //exe
            LPSTR(NULL) , //cmd
            LPSECURITY_ATTRIBUTES (NULL),
            LPSECURITY_ATTRIBUTES (NULL),
            BOOL                  (FALSE),
            DWORD                 (NORMAL_PRIORITY_CLASS),
            LPVOID                (NULL),
            LPCSTR                (NULL), //path
            LPSTARTUPINFOA        (&si),
            LPPROCESS_INFORMATION (&pi) );
			 
		PlaySound(TEXT("AudioLeap.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
			 
        break;

		
	/*
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);

		EndPaint(hwnd, &ps); 
		break;
	*/

	case WM_KEYDOWN:
		
		switch(wParam)
		{
							
        //Toggle code
        case VK_SPACE:
			{
				if(MUSIC_PLAY_STATUS==TRUE)
				{
					MUSIC_PLAY_STATUS=FALSE;
					PlaySound(NULL, NULL, SND_FILENAME | SND_ASYNC );
				}
				else
				{
					MUSIC_PLAY_STATUS=TRUE;
					PlaySound(TEXT("AudioLeap.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP );
				}
			}
			break;

            
            //Definite chief aim
			
			case 0x30:
			
			//CreateProcess("C:\\Program Files\\Nero\\Nero 7\\Core\\nero.exe", NULL , NULL, NULL, FALSE, CREATE_NO_WINDOW , NULL, NULL, &si, &pi);
			
			CreateProcessA(
            LPCSTR(TEXT("C:\\wnd\\DefChiefAim.exe")), //exe
            LPSTR(NULL) , //cmd
            LPSECURITY_ATTRIBUTES (NULL),
            LPSECURITY_ATTRIBUTES (NULL),
            BOOL                  (FALSE),
            DWORD                 (NORMAL_PRIORITY_CLASS),
            LPVOID                (NULL),
            LPCSTR                (NULL), //path
            LPSTARTUPINFOA        (&si),
            LPPROCESS_INFORMATION (&pi) );
			break;
		
		
		// Pointer seminar
		case 0x31: 
        
		CreateProcessA(
            LPCSTR(TEXT("C:\\wnd\\Inception_of_MSTC.exe")), //exe
            LPSTR(NULL) , //cmd
            LPSECURITY_ATTRIBUTES (NULL),
            LPSECURITY_ATTRIBUTES (NULL),
            BOOL                  (FALSE),
            DWORD                 (NORMAL_PRIORITY_CLASS),
            LPVOID                (NULL),
            LPCSTR                (NULL), //path
            LPSTARTUPINFOA        (&si),
            LPPROCESS_INFORMATION (&pi) );
			break;
		
		// What is computer
		case 0x32: 
        
		CreateProcessA(
            LPCSTR(TEXT("C:\\wnd\\Computer_Fundamental.exe")), //exe
            LPSTR(NULL) , //cmd
            LPSECURITY_ATTRIBUTES (NULL),
            LPSECURITY_ATTRIBUTES (NULL),
            BOOL                  (FALSE),
            DWORD                 (NORMAL_PRIORITY_CLASS),
            LPVOID                (NULL),
            LPCSTR                (NULL), //path
            LPSTARTUPINFOA        (&si),
            LPPROCESS_INFORMATION (&pi) );
			break;
		
		//Tool chain
		case 0x33: 
        
		CreateProcessA(
            LPCSTR(TEXT("C:\\wnd\\toolchain.exe")), //exe
            LPSTR(NULL) , //cmd
            LPSECURITY_ATTRIBUTES (NULL),
            LPSECURITY_ATTRIBUTES (NULL),
            BOOL                  (FALSE),
            DWORD                 (NORMAL_PRIORITY_CLASS),
            LPVOID                (NULL),
            LPCSTR                (NULL), //path
            LPSTARTUPINFOA        (&si),
            LPPROCESS_INFORMATION (&pi) );
			break;
			
		// Assembly to C	
		case 0x34: 
        
		CreateProcessA(
            LPCSTR(TEXT("C:\\wnd\\machine_asm_c.exe")), //exe
            LPSTR(NULL) , //cmd
            LPSECURITY_ATTRIBUTES (NULL),
            LPSECURITY_ATTRIBUTES (NULL),
            BOOL                  (FALSE),
            DWORD                 (NORMAL_PRIORITY_CLASS),
            LPVOID                (NULL),
            LPCSTR                (NULL), //path
            LPSTARTUPINFOA        (&si),
            LPPROCESS_INFORMATION (&pi) );
			break;
		
		
		
		
		
		// Group Formation
		case 0x35: 
        
		CreateProcessA(
            LPCSTR(TEXT("C:\\wnd\\project_groups.exe")), //exe
            LPSTR(NULL) , //cmd
            LPSECURITY_ATTRIBUTES (NULL),
            LPSECURITY_ATTRIBUTES (NULL),
            BOOL                  (FALSE),
            DWORD                 (NORMAL_PRIORITY_CLASS),
            LPVOID                (NULL),
            LPCSTR                (NULL), //path
            LPSTARTUPINFOA        (&si),
            LPPROCESS_INFORMATION (&pi) );
			
			break;
		
        //BST
		case 0x36: 
        
		CreateProcessA(
            LPCSTR(TEXT("C:\\wnd\\win32_bst.exe")), //exe
            LPSTR(NULL) , //cmd
            LPSECURITY_ATTRIBUTES (NULL),
            LPSECURITY_ATTRIBUTES (NULL),
            BOOL                  (FALSE),
            DWORD                 (NORMAL_PRIORITY_CLASS),
            LPVOID                (NULL),
            LPCSTR                (NULL), //path
            LPSTARTUPINFOA        (&si),
            LPPROCESS_INFORMATION (&pi) );
			break;
			
        //Quote
		case 0x37: 
        
		CreateProcessA(
            LPCSTR(TEXT("C:\\wnd\\Quote.exe")), //exe
            LPSTR(NULL) , //cmd
            LPSECURITY_ATTRIBUTES (NULL),
            LPSECURITY_ATTRIBUTES (NULL),
            BOOL                  (FALSE),
            DWORD                 (NORMAL_PRIORITY_CLASS),
            LPVOID                (NULL),
            LPCSTR                (NULL), //path
            LPSTARTUPINFOA        (&si),
            LPPROCESS_INFORMATION (&pi) );
			break;	
			
		//to_be_continued
		case 0x38: 
        
		CreateProcessA(
            LPCSTR(TEXT("C:\\wnd\\to_be_continued.exe")), //exe
            LPSTR(NULL) , //cmd
            LPSECURITY_ATTRIBUTES (NULL),
            LPSECURITY_ATTRIBUTES (NULL),
            BOOL                  (FALSE),
            DWORD                 (NORMAL_PRIORITY_CLASS),
            LPVOID                (NULL),
            LPCSTR                (NULL), //path
            LPSTARTUPINFOA        (&si),
            LPPROCESS_INFORMATION (&pi) );
			break;	
			
			
		//credits
		case 0x39: 
        
		CreateProcessA(
            LPCSTR(TEXT("C:\\wnd\\flame.exe")), //exe
            LPSTR(NULL) , //cmd
            LPSECURITY_ATTRIBUTES (NULL),
            LPSECURITY_ATTRIBUTES (NULL),
            BOOL                  (FALSE),
            DWORD                 (NORMAL_PRIORITY_CLASS),
            LPVOID                (NULL),
            LPCSTR                (NULL), //path
            LPSTARTUPINFOA        (&si),
            LPPROCESS_INFORMATION (&pi) );
			break;
		
		
		
		case VK_ESCAPE:
			PostQuitMessage(0);
			break;
		
		}


		
	break;
		
    
      

	case WM_DESTROY:
        ExitProcess(0);

		DeleteObject(hbmap);
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

