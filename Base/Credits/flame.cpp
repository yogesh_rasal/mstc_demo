#include <windows.h>
#include <stdio.h>

#pragma comment(lib,"user32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"gdi32.lib")
#pragma comment(lib,"winmm.lib")

#include "flame.h"

LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg, WPARAM wParam,LPARAM lParam);
FILE* debugFile = NULL;

void drawFlame(HDC hdc,POINT pt[]);
void initialize_points(POINT pt[],int cxClient,int cyClient,int flame_move,int flame_move3);

#define flame_size 16

int flag_appear[11];
int flag_move[11];

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine,int iCmdShow)
{
    static TCHAR szAppName[] = TEXT("Application");
    static TCHAR szCaption[] = TEXT("CPA-Project");

    HBRUSH hBrush =NULL;
    HICON hIcon = NULL;
    HICON hIconSm = NULL;
    HCURSOR hCursor = NULL;

    HWND hWnd;

    WNDCLASSEX wndEx;
    MSG msg;

    debugFile = fopen("debug.txt","w");
    if(debugFile == NULL)
    {
        MessageBox(NULL,TEXT("Cannot open debug file. Exitting.."),TEXT("Error"),MB_ICONERROR);
        ExitProcess(1);
    }

    hBrush = (HBRUSH)GetStockObject(BLACK_BRUSH);
    if(hBrush == NULL)
    {
        MessageBox((HWND)NULL,TEXT("Hbrush failed"),TEXT("GetStockObject"),MB_ICONERROR);
        return EXIT_FAILURE;
    }

    hCursor = LoadCursor((HINSTANCE)NULL,IDC_ARROW);
    if(hCursor == NULL)
    {
        MessageBox((HWND)NULL,TEXT("hCursor failed"),TEXT("LoadCursor"),MB_ICONERROR);
        return EXIT_FAILURE;
    }

    hIcon = LoadIcon((HINSTANCE)NULL,IDI_APPLICATION);
    if(hIcon == NULL)
    {
        MessageBox((HWND)NULL,TEXT("hIcon failed"),TEXT("LoadIcon"),MB_ICONERROR);
        return EXIT_FAILURE;
    }

    hIconSm = LoadIcon((HINSTANCE)NULL,IDI_APPLICATION);
    if(hIconSm == NULL)
    {
        MessageBox((HWND)NULL,TEXT("hIconSm failed"),TEXT("LoadIconSm"),MB_ICONERROR);
        return EXIT_FAILURE;
    }

    ZeroMemory(&wndEx,sizeof(WNDCLASSEX));
    ZeroMemory(&msg,sizeof(MSG));

    wndEx.cbSize = sizeof(WNDCLASSEX);
    wndEx.cbWndExtra = 0;
    wndEx.cbClsExtra = 0;

    wndEx.hbrBackground = hBrush;
    wndEx.hCursor = hCursor;
    wndEx.hIcon = hIcon;
    wndEx.hIconSm = hIconSm;

    wndEx.hInstance = hInstance;

    wndEx.lpszClassName = szAppName;
    wndEx.lpszMenuName = NULL;
    wndEx.lpfnWndProc = WndProc;

    wndEx.style = CS_HREDRAW | CS_VREDRAW;

    if(!RegisterClassEx(&wndEx))
    {
        MessageBox((HWND)NULL,TEXT("Cannot register class"),TEXT("RegisterClassEx"),MB_ICONERROR);
        return EXIT_FAILURE;
    }     

    hWnd = CreateWindowEx(WS_EX_APPWINDOW,szAppName,szCaption,WS_OVERLAPPEDWINDOW,0,0,800,600,NULL,NULL,hInstance,NULL);
    if(hWnd == NULL)
    {
        MessageBox((HWND)NULL,TEXT("Cannot get handle to Window"),TEXT("CreateWindowEx"),MB_ICONERROR);
        return EXIT_FAILURE;
    }

    ShowWindow(hWnd,iCmdShow);
    UpdateWindow(hWnd);

    while(GetMessage(&msg,NULL,0,0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg, WPARAM wParam,LPARAM lParam)
{
    HBRUSH hBrush;
    HDC hdc;
    PAINTSTRUCT ps;
    RECT rc;
    static int cxClient,cyClient;
    
    COLORREF color = RGB( 0, 0, 0 ); 
    static POINT pt[7];

    static float flame_move = 12;
    static float flame_move3 = 9;
    static float flame_move_flag1=0.1;
    static float flame_move_flag2=0.1;
    static float flame_move_flag3=0.1;

    static float y_move[11];

    static HFONT hFont = NULL;
    static HFONT hFont1 = NULL;
    static LOGFONT lf;
    static LOGFONT lf1;
    const char names[11][25] = {{"Apurva Kukade"}, {"Darshan Jogi"},{"Gaurav Shukl"},{"Nikita Sonsale"} ,{"Shraddha Gaikar"},{"Ravina Jain"},{"Renuka Hazare"},{"Tanvi Arawkar"},{"Tejas Kharche"},{"Yogeshwar Rasal"},{"Thank you Yogeshwar Sir"}};

    static int kay_chaluye = 0;
    static int flag_end=0;

    POINT poly_point[7] = {
                                    {2*cxClient/flame_size,5*cyClient/flame_size},
                                    {cxClient/flame_size,3*cyClient/flame_size},
                                    {2*cxClient/flame_size,4*cyClient/flame_size},
                                    {3*cxClient/flame_size,cyClient/flame_size},
                                    {5*cxClient/flame_size,3*cyClient/flame_size},
                                    {4*cxClient/flame_size,4*cyClient/flame_size},
                                    {4*cxClient/flame_size,5*cyClient/flame_size}
                                 };
    switch(uMsg)
    {
        case WM_CREATE:
            PlaySound("believer.wav",NULL,SND_FILENAME|SND_ASYNC);

            SetTimer(hWnd,ID_TIMER,40,NULL);
            SetTimer(hWnd,ID_TIMER_MOVE,5000,NULL);

            SetTimer(hWnd,ID_TIMER_APURVA, 10000,NULL);
            SetTimer(hWnd,ID_TIMER_DARSHAN,15000,NULL);
            SetTimer(hWnd,ID_TIMER_GAURAV,20000,NULL);
            SetTimer(hWnd,ID_TIMER_NIKITA,25000,NULL);
            SetTimer(hWnd,ID_TIMER_SHRADDHA,30000,NULL);
            SetTimer(hWnd,ID_TIMER_RAVINA,35000,NULL);
            SetTimer(hWnd,ID_TIMER_RENUKA,40000,NULL);
            SetTimer(hWnd,ID_TIMER_TANVI,45000,NULL);
            SetTimer(hWnd,ID_TIMER_TEJAS,50000,NULL);
            SetTimer(hWnd,ID_TIMER_YOGESHWAR,55000,NULL);
            SetTimer(hWnd,ID_TIMER_EXTRA,60000,NULL);    
            SetTimer(hWnd,ID_TIMER_THANKYOU,70000,NULL);
            SetTimer(hWnd,ID_TIMER_END,105000,NULL);

            ZeroMemory(&lf,sizeof(LOGFONT));

            lf.lfHeight = 20;
            lf.lfWidth = 20;
            lf.lfWeight = FW_BOLD;
            lf.lfItalic = 10;

            hFont = CreateFontIndirect(&lf);

            lf1.lfHeight = 30;
            lf1.lfWidth = 30;
            lf1.lfWeight = FW_BOLD;
            lf1.lfItalic = 10;

            hFont1 = CreateFontIndirect(&lf1);
            
            int i;
            for(i=0;i<11;i++)
                flag_appear[i] = 0;

            for(i=0;i<11;i++)
                y_move[i] = 1.0;

            for(i=0;i<11;i++)
                flag_move[i] = 0;        

            return 0;

        case WM_SIZE:
            cxClient = LOWORD(lParam);
            cyClient = HIWORD(lParam);

            initialize_points(pt,cxClient,cyClient,flame_move,flame_move3);

            return 0;    

        case WM_TIMER:

            switch(wParam)
            {
                case ID_TIMER:
                    
                    flame_move = flame_move-flame_move_flag1;
                    if(flame_move<8)
                    {
                        flame_move_flag1 = flame_move_flag1-0.05;
                    }
                    else
                    {
                        flame_move_flag1 = flame_move_flag1+0.05;
                    }

                    flame_move3 = flame_move3-flame_move_flag3;
                    if(flame_move3<8)
                    {
                        flame_move_flag3 = flame_move_flag3-0.05;
                    }
                    else
                    {
                        flame_move_flag3 = flame_move_flag3+0.05;
                    }

                    initialize_points(pt,cxClient,cyClient,flame_move,flame_move3);                    
                    InvalidateRect(hWnd,NULL,TRUE);
                    break;

                case ID_TIMER_MOVE:

                    for(i=0;i<10;i++)
                    {
                        if(flag_move[i] == 1)
                        {
                            y_move[i] = y_move[i] - 0.1f;
                        }
                    }

                    if(flag_move[10] == 1 )
                    {
                        if(kay_chaluye ==0)
                        {
                            kay_chaluye =1;
                            break;
                        }
                        y_move[10] = y_move[10]-0.1f;
                    }
                    
                    break;

                case ID_TIMER_APURVA:
                    flag_appear[0] = 1;
                    break; 

                case ID_TIMER_DARSHAN:
                    flag_appear[0] = 1;
                    flag_appear[1] = 1;
                    flag_move[0]=1;
                    break;

                case ID_TIMER_GAURAV:
                    flag_appear[0] = 1;
                    flag_appear[1] = 1;
                    flag_appear[2] = 1;
                    flag_move[0]=1;
                    flag_move[1]=1;
                    break;     

                case ID_TIMER_NIKITA:
                    flag_appear[0] = 1;
                    flag_appear[1] = 1;
                    flag_appear[2] = 1;
                    flag_appear[3] = 1;

                    flag_move[0]=1;
                    flag_move[1]=1;
                    flag_move[2]=1;
                    break;

                case ID_TIMER_SHRADDHA:
                    flag_appear[0] = 1;
                    flag_appear[1] = 1;
                    flag_appear[2] = 1;
                    flag_appear[3] = 1;
                    flag_appear[4] = 1;

                    flag_move[0]=1;
                    flag_move[1]=1;
                    flag_move[2]=1;
                    flag_move[3]=1;
                    break; 

                case ID_TIMER_RAVINA:
                    flag_appear[0] = 1;
                    flag_appear[1] = 1;
                    flag_appear[2] = 1;
                    flag_appear[3] = 1;
                    flag_appear[4] = 1;
                    flag_appear[5] = 1;

                    flag_move[0]=1;
                    flag_move[1]=1;
                    flag_move[2]=1;
                    flag_move[3]=1;
                    flag_move[4]=1;
                    break; 

                case ID_TIMER_RENUKA:
                    flag_appear[0] = 1;
                    flag_appear[1] = 1;
                    flag_appear[2] = 1;
                    flag_appear[3] = 1;
                    flag_appear[4] = 1;
                    flag_appear[5] = 1;
                    flag_appear[6] = 1;

                    flag_move[0]=1;
                    flag_move[1]=1;
                    flag_move[2]=1;
                    flag_move[3]=1;
                    flag_move[4]=1;
                    flag_move[5]=1;
                    break;

                 case ID_TIMER_TANVI:
                    flag_appear[0] = 1;
                    flag_appear[1] = 1;
                    flag_appear[2] = 1;
                    flag_appear[3] = 1;
                    flag_appear[4] = 1;
                    flag_appear[5] = 1;
                    flag_appear[6] = 1;
                    flag_appear[7] = 1;

                    flag_move[0]=1;
                    flag_move[1]=1;
                    flag_move[2]=1;
                    flag_move[3]=1;
                    flag_move[4]=1;
                    flag_move[5]=1;
                    flag_move[6]=1;
                    break; 

                case ID_TIMER_TEJAS:
                    flag_appear[0] = 1;
                    flag_appear[1] = 1;
                    flag_appear[2] = 1;
                    flag_appear[3] = 1;
                    flag_appear[4] = 1;
                    flag_appear[5] = 1;
                    flag_appear[6] = 1;
                    flag_appear[7] = 1;
                    flag_appear[8] = 1;

                    flag_move[0]=1;
                    flag_move[1]=1;
                    flag_move[2]=1;
                    flag_move[3]=1;
                    flag_move[4]=1;
                    flag_move[5]=1;
                    flag_move[6]=1;
                    flag_move[7]=1;
                    break;                

                case ID_TIMER_YOGESHWAR:
                    flag_appear[0] = 1;
                    flag_appear[1] = 1;
                    flag_appear[2] = 1;
                    flag_appear[3] = 1;
                    flag_appear[4] = 1;
                    flag_appear[5] = 1;
                    flag_appear[6] = 1;
                    flag_appear[7] = 1;
                    flag_appear[8] = 1;
                    flag_appear[9] = 1;

                    flag_move[0]=1;
                    flag_move[1]=1;
                    flag_move[2]=1;
                    flag_move[3]=1;
                    flag_move[4]=1;
                    flag_move[5]=1;
                    flag_move[6]=1;
                    flag_move[7]=1;
                    flag_move[8]=1;
                break;    

                case ID_TIMER_EXTRA:
                    flag_appear[0] = 1;
                    flag_appear[1] = 1;
                    flag_appear[2] = 1;
                    flag_appear[3] = 1;
                    flag_appear[4] = 1;
                    flag_appear[5] = 1;
                    flag_appear[6] = 1;
                    flag_appear[7] = 1;
                    flag_appear[8] = 1;
                    flag_appear[9] = 1;

                    flag_move[0]=1;
                    flag_move[1]=1;
                    flag_move[2]=1;
                    flag_move[3]=1;
                    flag_move[4]=1;
                    flag_move[5]=1;
                    flag_move[6]=1;
                    flag_move[7]=1;
                    flag_move[8]=1;
                    flag_move[9]=1;
                break;    

                case ID_TIMER_THANKYOU:
                    flag_appear[0] = 1;
                    flag_appear[1] = 1;
                    flag_appear[2] = 1;
                    flag_appear[3] = 1;
                    flag_appear[4] = 1;
                    flag_appear[5] = 1;
                    flag_appear[6] = 1;
                    flag_appear[7] = 1;
                    flag_appear[8] = 1;
                    flag_appear[9] = 1;
                    flag_appear[10] = 1;

                    flag_move[0]=1;
                    flag_move[1]=1;
                    flag_move[2]=1;
                    flag_move[3]=1;
                    flag_move[4]=1;
                    flag_move[5]=1;
                    flag_move[6]=1;
                    flag_move[7]=1;
                    flag_move[8]=1;
                    flag_move[9]=1;
                    flag_move[10]=1;   
                    break;  

                case ID_TIMER_END:
                    flag_end=1;
                    break;
            }
            
            return 0;

        case WM_PAINT:
            hdc = BeginPaint(hWnd,&ps);
            hBrush = CreateSolidBrush(RGB(255, 119, 0));
            SelectObject(hdc,(HBRUSH)hBrush);

            if(flag_end == 0)
            {
                

                drawFlame(hdc,pt);

                FillPath(hdc);

                SelectObject(hdc,(HFONT)hFont);
                
                SetBkMode(hdc,TRANSPARENT);
                SetTextColor(hdc,color);
                SetTextAlign(hdc,TA_CENTER|TA_BOTTOM);

                for(i=0;i<11;i++)
                {
                    if(flag_appear[i] == 1)
                    {
                        TextOut(hdc,8*cxClient/16,y_move[i]* 6*cyClient/8,names[i],lstrlen(names[i]));
                        KillTimer(hWnd,i+2);
                    }    
                }
                
                EndPath(hdc);
                EndPaint(hWnd,&ps);
                DeleteObject(hBrush);
            }
            else if(flag_end == 1)
            {
                SelectObject(hdc,(HBRUSH)hBrush);
                SelectObject(hdc,(HFONT)hFont1);
                SetTextAlign(hdc,TA_CENTER);
                TextOut(hdc,cxClient/2,cyClient/2,TEXT("Special thanks to Radhika Mam :)"),lstrlen("Special thanks to Radhika Mam :)"));
            }
            
            return 0;

        case WM_DESTROY:
            KillTimer(hWnd,ID_TIMER);
            KillTimer(hWnd,ID_TIMER_END);
            KillTimer(hWnd,ID_TIMER_EXTRA);
            KillTimer(hWnd,ID_TIMER_MOVE);
            PostQuitMessage(0);
            break;
    }

    return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

void drawFlame(HDC hdc,POINT pt[])
{
    BeginPath(hdc);
    PolyBezier(hdc,pt,7);

    MoveToEx(hdc,pt[0].x,pt[0].y,NULL);
    /*LineTo(hdc,pt[1].x,pt[1].y);

    MoveToEx(hdc,pt[1].x,pt[1].y,NULL);
    LineTo(hdc,pt[2].x,pt[2].y);

    MoveToEx(hdc,pt[2].x,pt[2].y,NULL);
    LineTo(hdc,pt[3].x,pt[3].y);

    MoveToEx(hdc,pt[3].x,pt[3].y,NULL);
    LineTo(hdc,pt[4].x,pt[4].y);

    MoveToEx(hdc,pt[4].x,pt[4].y,NULL);
    LineTo(hdc,pt[5].x,pt[5].y);

    MoveToEx(hdc,pt[5].x,pt[5].y,NULL);*/
    LineTo(hdc,pt[6].x,pt[6].y);

    EndPath(hdc);
}

void initialize_points(POINT pt[],int cxClient,int cyClient,int flame_move,int flame_move3)
{
    //Beginning point
            pt[0].x = 6*cxClient/flame_size;
            pt[0].y = 15*cyClient/flame_size;

            //1st control pt
            pt[1].x = 3*cxClient/flame_size;
            pt[1].y = 9*cyClient/flame_size;

            //2nd Control pt
            pt[2].x = flame_move*cxClient/flame_size;
            pt[2].y = flame_move*cyClient/flame_size;


            //3rd control pt
            pt[3].x = flame_move3*cxClient/flame_size;
            pt[3].y = (flame_move3/3)*cyClient/flame_size;

            //4th control pt
            pt[4].x = 15*cxClient/flame_size;
            pt[4].y = 9*cyClient/flame_size;

            //5th control pt
            pt[5].x = flame_move*cxClient/flame_size;
            pt[5].y = flame_move*cyClient/flame_size;

            //End pt
            pt[6].x = 10*cxClient/flame_size;
            pt[6].y = 15*cyClient/flame_size;
}
// hRgn = CreatePolygonRgn(pt,7,WINDING);
            // if(hRgn == NULL)
            // {
            //     MessageBox(NULL,TEXT("Cannot create hRgn"),TEXT("Error"),MB_ICONERROR);
            //     ExitProcess(1);
            // }
            // FillRgn(hdc,hRgn,(HBRUSH)GetStockObject(GRAY_BRUSH));
            
            //Rectangle(hdc,(counter)  * cxClient/4,cyClient/4,(counter) * 3*cxClient/4,3*cyClient/4);
            