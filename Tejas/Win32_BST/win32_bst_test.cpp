//////Basic Win32 Binary Search Tree//////
///// Developed By : Tejas Kharche  ////


#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "global.h"
#include "win32_bst.h"


// libpath 
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")

FILE *g_file = NULL;

//Global VAR;
bst_t * p_bst;

int Arr[20]= { 50,21,3,41,53,712,812,921,1,0,31,27,513,115,5654,758,23,450,2376,7676};
int size_of_arr=sizeof(Arr)/sizeof(Arr[0]);

//int NUM_BST_NODE_DISPLAYED=0;
//2. Hollywood Principal

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);



//3
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow) //ncmwindow show the window on how it should be shown e.g. maximized minimized or hidden etc.
{
	fopen_s(&g_file, "log.txt","w");
    fprintf(g_file,"file created");
	
	p_bst=create_bst();  //tree initialization
	
	
	/*
	int Arr[20]= { 50,21,3,41,53,712,812,921,1,0,31,27,513,115,5654,758,23,450,2376,7676};
	//int Arr[20]={ 50,21,3,41,53,6,712,812,921,1,31,513,115,5654,758,5,23,1254545,2376,7676};
	size_of_arr=sizeof(Arr)/sizeof(Arr[0]);
	
		for(NUM_BST_NODE_DISPLAYED=0;NUM_BST_NODE_DISPLAYED<size_of_arr;NUM_BST_NODE_DISPLAYED++){
			tree_insert(p_bst,Arr[NUM_BST_NODE_DISPLAYED]);				
		}
		*/
	
	
	//4. Data
	WNDCLASSEX wndclass; //  style
	HWND hwnd;	// Handle == Pointer to Window
	MSG msg;	//Current msg
	TCHAR szAppName[] = TEXT("Win32 Binary Search Tree"); // name of class 
    TCHAR szWindowCaption[] = TEXT("Win32 Binary Search Tree");

	//5. Code : Fill the struct 
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0; // additional
	wndclass.cbWndExtra = 0; // additional
	wndclass.lpfnWndProc = WndProc; // it actully funcptr
	wndclass.hInstance = hInstance; // 1st param

	wndclass.hIcon = LoadIcon(NULL, IDI_SHIELD); // Custom icon for Taskbar
	wndclass.hIconSm = LoadIcon(NULL, IDI_SHIELD); // minimize icon for window itself
	wndclass.hCursor = LoadCursor(NULL, IDC_HAND);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	wndclass.lpszClassName = szAppName; // Class Name ""
	wndclass.lpszMenuName = NULL; // used in project

	// 6
	RegisterClassEx(&wndclass);

	// 7  Create window and return its
	hwnd = CreateWindow(szAppName, // class style
        szWindowCaption,    //Windows Caption name
		WS_OVERLAPPEDWINDOW,	   // Style of wnd
		CW_USEDEFAULT,				// x cord
		CW_USEDEFAULT,				// y cord
		CW_USEDEFAULT,				// width
		CW_USEDEFAULT,				// height
		(HWND)NULL,                 // hWndParent
        (HMENU)NULL,				// hMenu
		hInstance,					// handle Instance of current application
		NULL);						// No lpParam (used for additional data)

	// 8  memset
	ShowWindow(hwnd, SW_MAXIMIZE);
	UpdateWindow(hwnd); // VIMP

	// 9. Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg); //from here OS call's our callback function ie WinProc in this application
	}

	
           
	w_destroy_bst(&p_bst);  //tree desctuction
	// 10
	
	fclose(g_file);
    g_file = NULL;
	return((int)msg.wParam);
}

// 11 a 
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	 static int cxClient, cyClient, cxChar, cyChar,cxCaps,iMaxWidth; 
	 int i,x,y,iVertPos, iHorzPos, iPaintBeg, iPaintEnd; 
	 HDC hdc; 
	 TEXTMETRIC tm; 
	 PAINTSTRUCT ps;
	 
	// 11 b
	switch (iMsg)
	{
		// very 1st Msg
	case WM_CREATE:
		//MessageBox(NULL, TEXT("Window Created!"), TEXT("Tejas's 1st Window"), MB_OK);
		hdc = GetDC(hwnd); 
        GetTextMetrics(hdc, &tm);
        cxChar = tm.tmAveCharWidth; 
        cyChar = tm.tmHeight + tm.tmExternalLeading;
		CLIENT_CHAR_X=cxChar;
		CLIENT_CHAR_Y=cyChar;
        //SetTimer(hwnd,ID_TIMER_BST_DISPLAY,1000,NULL);
		
		SetTimer(hwnd,ID_TIMER_BST_DISPLAY,2000,NULL);
		
		ReleaseDC(hwnd, hdc); 
        hdc = NULL; 
		break;
	
	case WM_SIZE: 
            cxClient = LOWORD(lParam); 
            cyClient = HIWORD(lParam); 
            CLIENT_XArea = cxClient;
            CLIENT_YArea = cyClient;
			IS_TREE_DIPLAYED=0;       //after resize render tree again
			TREE_TRAVERSAL_MODE = 1;  //after resize operation we need to draw agin all sequences
		   break;  
		
	
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		
	
		if(NUM_BST_NODE_DISPLAYED != size_of_arr)
		{
			tree_insert(p_bst,Arr[NUM_BST_NODE_DISPLAYED]);				
		}
			
		w_bst_display_r(hdc,p_bst);
		//IS_TREE_DIPLAYED=1;
		
		if(NUM_BST_NODE_DISPLAYED == size_of_arr)
		{
			// Print Pre Order sequence
				w_preorder_r(hdc,p_bst);
				PRE_ORDER_DISPLAYED=TRUE;
			
			//Print in order sequence
			
				w_inorder_r(hdc,p_bst);
				IN_ORDER_DISPLAYED=TRUE;
			//Print post order sequence
	
				w_postorder_r(hdc,p_bst);
				POST_ORDER_DISPLAYED=TRUE;
		}
		
		EndPaint(hwnd, &ps); 
        hdc = NULL; 
		break;
		
		
	case WM_TIMER:
		
		switch(wParam)
		{
			case ID_TIMER_BST_DISPLAY:
				
			if(NUM_BST_NODE_DISPLAYED != size_of_arr)
			{			
				
				NUM_BST_NODE_DISPLAYED++;
				InvalidateRect(hwnd,NULL,FALSE);
				//IS_TREE_DIPLAYED=0;
			}
			
			break;
		}
	
		break;
		//return 0;
	
	case WM_DESTROY:
		//MessageBox(NULL, TEXT("Window Destroyed"), TEXT("Tejas's 1st Window"), MB_ICONERROR);
		PostQuitMessage(0);
		break;
	}

	
	// 12
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void _w_inorder_r(HDC hdc,bst_node_t * p_node)
{	
	char message_buffer[20];
	char new_message_buffer[25];  // message+, "Semicolon"
	if(p_node == NULL )
		return;
		
	_w_inorder_r(hdc,p_node->left);
	//Action start
	itoa(p_node->data,message_buffer,10);
	SetTextAlign(hdc, TA_LEFT | TA_TOP); 
	SetBkMode(hdc,TRANSPARENT);
	SetTextColor(hdc,green);
	sprintf(new_message_buffer,"[%s]",message_buffer);
	//TextOut(hdc, IN_ORDER_START_X_POSITION , IN_ORDER_START_Y_POSITION, message_buffer, lstrlen(message_buffer));
	TextOut(hdc, IN_ORDER_START_X_POSITION , IN_ORDER_START_Y_POSITION, new_message_buffer, lstrlen(new_message_buffer));
	//IN_ORDER_START_X_POSITION=IN_ORDER_START_X_POSITION+ (lstrlen(message_buffer) + 1)* CLIENT_CHAR_X;
	IN_ORDER_START_X_POSITION=IN_ORDER_START_X_POSITION+ (lstrlen(new_message_buffer) + 1)* CLIENT_CHAR_X;
	//Action End
	_w_inorder_r(hdc,p_node->right);
}

void _w_preorder_r(HDC hdc,bst_node_t * p_node)
{
	char message_buffer[20];
	char new_message_buffer[25];  // message+, "Semicolon"
	if(p_node == NULL )
		return;
		
	//Action start
	itoa(p_node->data,message_buffer,10);
	SetTextAlign(hdc, TA_LEFT | TA_TOP); 
	SetBkMode(hdc,TRANSPARENT);
	SetTextColor(hdc,orange);
	
	//TextOut(hdc, PRE_ORDER_START_X_POSITION , PRE_ORDER_START_Y_POSITION, message_buffer, lstrlen(message_buffer));
	sprintf(new_message_buffer,"[%s]",message_buffer);
	TextOut(hdc, PRE_ORDER_START_X_POSITION , PRE_ORDER_START_Y_POSITION, new_message_buffer, lstrlen(new_message_buffer));
	//PRE_ORDER_START_X_POSITION=PRE_ORDER_START_X_POSITION+ (lstrlen(message_buffer) + 1)* CLIENT_CHAR_X;
	
	PRE_ORDER_START_X_POSITION=PRE_ORDER_START_X_POSITION+ (lstrlen(new_message_buffer)+1)* CLIENT_CHAR_X;
	
	//Action End
	_w_preorder_r(hdc,p_node->left);
	_w_preorder_r(hdc,p_node->right);
}


void _w_postorder_r(HDC hdc,bst_node_t * p_node)
{
	char message_buffer[20];
	char new_message_buffer[25];  // message+, "Semicolon"
	if(p_node == NULL )
		return;
		
	_w_postorder_r(hdc,p_node->left);
	_w_postorder_r(hdc,p_node->right);
	//Action startss
	itoa(p_node->data,message_buffer,10);
	SetTextAlign(hdc, TA_LEFT | TA_TOP); 
	SetBkMode(hdc,TRANSPARENT);
	SetTextColor(hdc,skyblue);
	//TextOut(hdc, POST_ORDER_START_X_POSITION , POST_ORDER_START_Y_POSITION, message_buffer, lstrlen(message_buffer));
	sprintf(new_message_buffer,"[%s]",message_buffer);
	TextOut(hdc, POST_ORDER_START_X_POSITION , POST_ORDER_START_Y_POSITION, new_message_buffer, lstrlen(new_message_buffer));
	//POST_ORDER_START_X_POSITION=POST_ORDER_START_X_POSITION+ (lstrlen(message_buffer) + 1)* CLIENT_CHAR_X;
	POST_ORDER_START_X_POSITION=POST_ORDER_START_X_POSITION+ (lstrlen(new_message_buffer) + 1)* CLIENT_CHAR_X;
	//Action End
}


void _w_bst_display_r(HDC hdc,bst_node_t * p_node)
{
	HPEN hNodeLinkPen;
	HPEN hOldPen;
	char message_buffer[20];
	char message_buffer1[100];
	if(p_node == NULL )
		return;
		
	_w_bst_display_r(hdc,p_node->left);
	_w_bst_display_r(hdc,p_node->right);
	
	itoa(p_node->data,message_buffer,10);
	
	sprintf(message_buffer1,"Inside RECURISVE DISPLAY %s \n",message_buffer);
	fprintf(g_file,message_buffer1);
	
	if(p_node->parent != NULL) //line between parent node and child
	{
		if(p_node == p_node->parent->left)  //node is left then color link in skyblue
		{
			hNodeLinkPen=CreatePen(PS_SOLID,2,yellow);	
		}
		else    //node is right then color link in red
		{
			hNodeLinkPen=CreatePen(PS_SOLID,2,red);
		}
		
		hOldPen=(HPEN)SelectObject(hdc,hNodeLinkPen);
		//MoveToEx(hdc,TREE_SPAWN_X+p_node->parent->x,TREE_SPAWN_Y+p_node->parent->y,NULL);
		//LineTo(hdc,TREE_SPAWN_X+(p_node->x),TREE_SPAWN_Y+(p_node->y));
		
		MoveToEx(hdc,TREE_SPAWN_X+(p_node->x),TREE_SPAWN_Y+(p_node->y),NULL);
		LineTo(hdc,TREE_SPAWN_X+p_node->parent->x,TREE_SPAWN_Y+p_node->parent->y);
		
		DeleteObject(hNodeLinkPen);
		SelectObject(hdc,hOldPen);
	}
	w_PrintBstNode(hdc, TREE_SPAWN_X +p_node->x,TREE_SPAWN_Y +p_node->y, 30,message_buffer);
	
	//w_PrintBstNode(hdc, CLIENT_XArea/2 +rand()/(CLIENT_XArea),CLIENT_YArea/2+ rand()/(CLIENT_YArea), 30,message_buffer);
}


void _w_destroy_bst_r(bst_node_t * p_node)
{
	if(p_node == NULL)
		return;
		
	_w_destroy_bst_r(p_node->left);
	_w_destroy_bst_r(p_node->right);
	free(p_node);
	p_node=NULL;
	//printf("[%d]<->",p_node->data);
}


bst_node_t * getBstNode(int in_data)
{
	bst_node_t * p_node=NULL;
	//p_node=(bst_node_t *)xcalloc(1,sizeof(bst_node_t));
	p_node=(bst_node_t *)xmalloc(sizeof(bst_node_t));
	if(p_node == NULL)
		return(NULL);
	
	p_node->data=in_data;
	p_node->parent=NULL;
	p_node->right=NULL;
	p_node->left=NULL;
	return (p_node);
}


void * xmalloc(size_t size_in_bytes)
{
	void * p=NULL;
	p=malloc(size_in_bytes);
	if(p== NULL)
	{
		fprintf(stderr,"Memory Allocation Failed");
	}
	return(p);	
}


//----------------------------------------INTERFACE FUNCTIONS----------------------------------------------------

bst_t * create_bst()
{
	bst_t * p_bst=NULL;
	//p_bst=(bst_t *)xcalloc(1,sizeof(bst_t));
	
	p_bst=(bst_t *)xmalloc(sizeof(bst_t));
	if(p_bst == NULL)
	{
		fprintf(stderr,"Tree Creation Failed\n");
		return(NULL);
	}
	
	p_bst->p_root=NULL;
	p_bst->nr_element=0;
	
	return(p_bst);
}



status_t tree_insert(bst_t* p_bst, data_t in_data)
{
    bst_node_t* p_new_node = NULL; 
    bst_node_t* p_run = NULL; 

    p_new_node = getBstNode(in_data); 
    p_run = p_bst->p_root; 
    
    if(p_run == NULL)  //inserting Root Node 
    {
        p_bst->p_root = p_new_node; 
		p_new_node->height=1;
        p_bst->nr_element++; 
        return (SUCCESS); 
    }

    while(TRUE)
    {
        if(in_data <= p_run->data)
        {
            if(p_run->left == NULL)
            {
                p_run->left = p_new_node; 
                p_new_node->parent = p_run; 
				p_new_node->height=p_run->height+1;  //child node has height as parent + 1
                p_bst->nr_element++; 
				
				//while inserting node make the node's position respective to it's parent
				p_new_node->x=p_new_node->parent->x-TREE_X_OFFSET;
				p_new_node->y=p_new_node->parent->y+TREE_Y_OFFSET;
                return (SUCCESS); 
            }
            else
            {
                p_run = p_run->left; 
                continue;     
            }
        }
        else
        {
            if(p_run->right == NULL)
            {
                p_run->right = p_new_node; 
                p_new_node->parent = p_run; 
                p_bst->nr_element++; 
				p_new_node->height=p_run->height+1; 
				//while inserting node make the node's position respective to it's parent
				p_new_node->x=p_new_node->parent->x+TREE_X_OFFSET;
				p_new_node->y=p_new_node->parent->y+TREE_Y_OFFSET;
				
                return (SUCCESS); 
            }
            else
            {
                p_run = p_run->right; 
                continue; 
            }
        }
    }
}

void w_inorder_r(HDC hdc,bst_t * p_bst)
{	
	//INSURING THAT GLOBAL VALUES ARE IN SYNC WITH ORIGINAL VALUES THEY ARE NOT MODIFIED
	IN_ORDER_START_X_POSITION=ORG_IN_ORDER_START_X_POSITION;
	IN_ORDER_START_Y_POSITION=ORG_IN_ORDER_START_Y_POSITION;

	
	char * message="IN-ORDER:";	
	
	SetTextAlign(hdc, TA_LEFT | TA_TOP); 
	SetBkMode(hdc,TRANSPARENT);
	SetTextColor(hdc,green);
	TextOut(hdc,ORG_IN_ORDER_START_X_POSITION - 15 * CLIENT_CHAR_X  , ORG_IN_ORDER_START_Y_POSITION, message, lstrlen(message));
	
	//printf("[START]<->");
	_w_inorder_r(hdc,p_bst->p_root);
	//puts("[END]");
	
	//Restoing GLOBAL VAR WITH ORIGINAL VALUES
	
	IN_ORDER_START_X_POSITION=ORG_IN_ORDER_START_X_POSITION;
	IN_ORDER_START_Y_POSITION=ORG_IN_ORDER_START_Y_POSITION;
}

void w_preorder_r(HDC hdc,bst_t * p_bst)
{	
	//INSURING THAT GLOBAL VALUES ARE IN SYNC WITH ORIGINAL VALUES THEY ARE NOT MODIFIED
	PRE_ORDER_START_X_POSITION=ORG_PRE_ORDER_START_X_POSITION;
	PRE_ORDER_START_Y_POSITION=ORG_PRE_ORDER_START_Y_POSITION;

	
	char * message="PRE-ORDER:";
	
	SetTextAlign(hdc, TA_LEFT | TA_TOP); 
	SetBkMode(hdc,TRANSPARENT);
	SetTextColor(hdc,orange);
	TextOut(hdc,ORG_PRE_ORDER_START_X_POSITION - 15 * CLIENT_CHAR_X  , ORG_PRE_ORDER_START_Y_POSITION, message, lstrlen(message));
	
	//printf("[START]<->");
	_w_preorder_r(hdc,p_bst->p_root);
	//puts("[END]");
	
	//Restoing GLOBAL VAR WITH ORIGINAL VALUES
	
	PRE_ORDER_START_X_POSITION=ORG_PRE_ORDER_START_X_POSITION;
	PRE_ORDER_START_Y_POSITION=ORG_PRE_ORDER_START_Y_POSITION;

}


void w_bst_display_r(HDC hdc,bst_t * p_bst)
{
	HFONT HOldFont,HNewFont;
	
	HNewFont=CreateFont(50, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Cambria"));
	HOldFont=(HFONT)SelectObject(hdc, HNewFont);
	char * message="Binary Search Tree";	
	SetTextAlign(hdc, TA_LEFT | TA_TOP); 
	SetBkMode(hdc,TRANSPARENT);
	SetTextColor(hdc,cyan);
	TextOut(hdc,20 , 20, message, lstrlen(message));
	
	SelectObject(hdc, HOldFont);  //Restore old font
	fprintf(g_file,"Inside Display Tree Interface Function");
	_w_bst_display_r(hdc,p_bst->p_root);
	
	
}

void w_postorder_r(HDC hdc,bst_t * p_bst)
{
	//INSURING THAT GLOBAL VALUES ARE IN SYNC WITH ORIGINAL VALUES THEY ARE NOT MODIFIED
	POST_ORDER_START_X_POSITION=ORG_POST_ORDER_START_X_POSITION;
	POST_ORDER_START_Y_POSITION=ORG_POST_ORDER_START_Y_POSITION;

	
	char * message="POST-ORDER:";
	
	//printf("[START]<->");
	_w_postorder_r(hdc,p_bst->p_root);
	//puts("[END]");
	SetTextAlign(hdc, TA_LEFT | TA_TOP); 
	SetBkMode(hdc,TRANSPARENT);
	SetTextColor(hdc,skyblue);
	TextOut(hdc,ORG_POST_ORDER_START_X_POSITION - 15 * CLIENT_CHAR_X , ORG_POST_ORDER_START_Y_POSITION, message, lstrlen(message));
	
	//Restoing GLOBAL VAR WITH ORIGINAL VALUES
	
	POST_ORDER_START_X_POSITION=ORG_POST_ORDER_START_X_POSITION;
	POST_ORDER_START_Y_POSITION=ORG_POST_ORDER_START_Y_POSITION;

	
}

status_t w_destroy_bst(bst_t **pp_bst)
{
	_w_destroy_bst_r((*pp_bst)->p_root);
	free(*pp_bst);
	*pp_bst=NULL;
	return(SUCCESS);
}


bool w_PrintBstNode(HDC hdc,int iXCenter,int iYCenter,int radius,char * message)
{
	char buffer[50];
	
	bool status = FALSE;
	status = Ellipse(hdc,(iXCenter-radius),(iYCenter-radius),(iXCenter+radius),(iYCenter+radius));
	if( status == TRUE && message != NULL)
	{
		//HPEN hNewPen;
		//HPEN hOldPen;
		 
		//hNewPen=CreatePen(PS_SOLID, 2 , green);
		//hOldPen=(HPEN)SelectObject(hdc,hNewPen);
		
		SetTextAlign(hdc, TA_CENTER | TA_TOP); 
		SetBkMode(hdc,TRANSPARENT);
		SetTextColor(hdc,red);
        TextOut(hdc,iXCenter , iYCenter-CLIENT_CHAR_Y/2, message, lstrlen(message)+1);
		
		//SelectObject(hdc,hOldPen);
		//DeleteObject(hNewPen);
	}
	sprintf(buffer,"Inside PrintNode() %s \n",message);
	fprintf(g_file,buffer);
    return(status);
}



void _w_shift_left_r(bst_node_t * p_node)
{
	if(p_node == NULL)
		return;
	//sift left in in-order order
	_w_shift_left_r(p_node->left);
	_w_shift_left_r(p_node->right);
	p_node->x=p_node->x-TREE_SHIFT_X_OFFSET;
}

void _w_shift_right_r(bst_node_t * p_node)
{
	if(p_node == NULL)
		return;
	//sift left in in-order order
	
	_w_shift_right_r(p_node->left);
	_w_shift_right_r(p_node->right);
	p_node->x=p_node->x+TREE_SHIFT_X_OFFSET;
	
}


int MIN (int X, int Y)  
{
  return ((X) < (Y)) ? (X) : (Y);
}

int MAX (int X, int Y)  
{
  return ((X) > (Y)) ? (X) : (Y);
}



int w_find_max_height(bst_t *p_bst)
{
	bst_node_t * p_run=p_bst->p_root;
	int o_max=0;
	_w_find_max_height_r(p_run,&o_max);
	
	return(o_max);
	
}


void _w_find_max_height_r(bst_node_t * p_node,int * o_max)
{
	if(p_node == NULL)
		return;
		
	_w_find_max_height_r(p_node->left,o_max);
	_w_find_max_height_r(p_node->right,o_max);
	*o_max=MAX(p_node->height,*o_max);
	
	//printf("[%d]<->",p_node->data);
}
